// Programa que muestra el uso de switch statement

#include <iostream>
using namespace std;

int main(){
int num=0;
cout << "Ingresa el idioma en que gustas ver el saludo:"<<endl;
cout << "1: Aleman"  << endl;
cout << "2: Ingles"  << endl;
cout << "3: Español" << endl;
cout << "4: Japones" << endl;
cout << ": ";
cin  >> num;

 switch(num){

case 1:
	cout << "Guten Morgen"<<endl;
 	break;
case 2:
	cout <<"Good Morning"<< endl;
	break;
case 3: 
	cout << "Buenos Días" << endl;
	break;
case 4: 
	cout << "Konnichiwa"<<endl;
	break;

}

}
